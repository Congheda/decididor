const formTemplate = document.createElement('template')
formTemplate.innerHTML = `
<style>
form{
    margin: 0 auto;
    width: 740px
    height: 498px;
    position: relative;
}

input{
    padding: 0 10 px;
    margin: 0;
    width: 240px;
    background: 255 255 255;
}

label{
    font-weight: bold;
}

</style>
<form name="deciderForm" id="deciderForm">
<label for="form" >Introduce una opción:</label>

<option-input></option-input>
<option-button></option-button>

</form>
`

class DeciderForm extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(formTemplate.content.cloneNode(true))
        this.input = this.shadowDOM.getElementById("deciderForm")
        this.options = []
    }

    connectedCallback() {
        document.body.addEventListener("addOption", this.decider.bind(this))//investigar bind
    }
    
    decider() {
        //result = this.input.value
        this.options.push(this.input.value)
        this.input.value= ''
        this.input.focus()
        console.log(this.options)
    }
}

window.customElements.define("decider-form", DeciderForm)