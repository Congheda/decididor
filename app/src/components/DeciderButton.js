const buttonTemplate = document.createElement('template')
buttonTemplate.innerHTML = `
<style>
    button{
        text-decoration: none;
        padding: 5px;
        font-weight: 500;
        font-size: 15px;
        margin-top: 20px;
        color: #f8096e;
        background-color: black;
        border-radius: 7px 0% 7px 0%;
        box-shadow: 3px 3px #f79ac0;
        font-weight: bold
    }
</style>
<button type="submit">El Decididor decide...</button>
`

class DeciderButton extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(buttonTemplate.content.cloneNode(true))
        this.button = this.shadowDOM.querySelector('button')
    }

    connectedCallback() {
        this.button.addEventListener('click', () =>
            window.alert("En construcción")
        )
    }



}

window.customElements.define("decider-button", DeciderButton,)