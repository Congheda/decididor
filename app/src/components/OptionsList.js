const listTemplate = document.createElement('template')
listTemplate.innerHTML = `
<style>
ul{
    list-style-type: circle;
    list-style-position: inside;
    text-align: left;
    text-decoration: none;
    padding: 5px;
    font-weight: 500;
    font-size: 15px;
    color: black;
    font-weight: bold;
    background-color: #f79ac0;
    border-radius: 7px 0% 7px 0%;
    box-shadow: 3px 3px;
    width: 500px;
    left: -500px;
    margin: 0 auto;
    margin-top: 25px; 
}

</style>
<ul>
<li>Holi </li>
<li>Probando </li>
<li>Opción 3 </li>
<li>Opción 4 </li>
<li>Opción 5 </li>
</ul>
`
class OptionsList extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(listTemplate.content.cloneNode(true))
    }
}

window.customElements.define("options-list", OptionsList)