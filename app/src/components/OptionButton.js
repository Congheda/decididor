const OpBuTemplate = document.createElement('template')
OpBuTemplate.innerHTML = `
<style>
    button{
        text-decoration: none;
        padding: 5px;
        font-weight: 500;
        font-size: 15px;
        color: black;
        background-color: #f79ac0;
        border-radius: 7px 0% 7px 0%;
        box-shadow: 3px 3px;
        font-weight: bold
        
    }
</style>
<button type="submit" id="optionButton">Añadir opción</button>
`

class OptionButton extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(OpBuTemplate.content.cloneNode(true))
        this.button = this.shadowDOM.querySelector('button')
    }

    connectedCallback() {
        this.button.addEventListener('click', () =>
        this.dispatchEvent(
            new CustomEvent('addOption', {
                bubbles: true,
                detail: this.shadowDOM.getElementById("optionButton"),
                composed: true
            })
        ) 
        )
    }}



window.customElements.define("option-button", OptionButton)