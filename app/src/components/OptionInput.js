const inputTemplate = document.createElement('template')
inputTemplate.innerHTML = `
<style>
input::placeholder {
    color: white;
    font-size:1.5em;
}
</style>
<input placeholder="text" id="optionInput">
`
class OptionInput extends HTMLElement {
    constructor() {
        super()
        this.shadowDOM = this.attachShadow({ mode: 'open' })
        this.shadowDOM.appendChild(inputTemplate.content.cloneNode(true))
        this.input=this.shadowDOM.querySelector('input')
    }
    connectedCallback() {
        this.input.addEventListener('change', () =>
        this.dispatchEvent(
            new CustomEvent('addOption', {
                bubbles: false,
                detail: this.shadowDOM.getElementById("optionInput"),
                composed: true
            })
            )
        ) 
        
    }
}


window.customElements.define("option-input", OptionInput)